import json
import os

from cloudant.client import Cloudant
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def root():
    credspath = os.path.dirname(os.path.abspath(__file__)) + '/creds.json'

    creds = json.load(open(credspath))

    dbname = creds['dbname']
    docname = creds['docname']
    dbuser = creds['username']
    dbpass = creds['password']
    dburl = 'https://' + creds['host']
    dbclient = Cloudant(dbuser, dbpass, url=dburl, connect=True)

    db = dbclient[dbname]
    settings = db['settings']
    api_id = settings['api_id']
    api_hash = settings['api_hash']
    session = settings['session']
    keywords = settings['keywords']
    sleeptimer = settings['sleeptimer']
    output_channel = settings['output_channel']

    doc = db[docname]
    channels = doc['channels']
    lastupdate = doc['lastupdate']

    return render_template('01.html', channels=channels, lastupdate=lastupdate)

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=False, port=8080)
